$(function() {
  $("#datepicker").datepicker({
    autoclose: true,
    todayHighlight: true
  }).datepicker('update', (document.getElementById("dob").value == "") ? new Date() : document.getElementById("dob").value);
});
var f = document.getElementsByTagName("form")[0];
f.addEventListener("submit", function(event) {
  console.log(document.getElementById("dob").value);
  var error_data = false;
  // FirstName
  if (this["firstName"].value.length <= 3) {
    error_data = true;
    this["firstName"].nextElementSibling.innerHTML = "Enter minimum 4 characters";
  } else {
    this["firstName"].nextElementSibling.innerHTML = "";
  }
  var dob = this["dob"].value;
  var datepart = dob.split('-');
  var year = parseInt(datepart[0]);
  var month = parseInt(datepart[1]) - 1;
  var day = parseInt(datepart[2]);
  var today = new Date();
  var age = today.getFullYear() - year;
  if (today.getMonth() < month || (today.getMonth() == month && today.getDate() < day)) {
    age--;
  }
  // LastName
  if (age < 18) {
    error_data = true;
    this["dob"].nextElementSibling.innerHTML = "Minimum 18 years of age";
  } else {
    this["dob"].nextElementSibling.innerHTML = "";
  }
  if (this["lastName"].value.length <= 3) {
    error_data = true;
    this["lastName"].nextElementSibling.innerHTML = "Enter minimum 4 characters";
  } else {
    this["lastName"].nextElementSibling.innerHTML = "";
  }
  // About
  if (this["about"].value.length <= 10) {
    error_data = true;
    this["about"].nextElementSibling.innerHTML = "Enter minimum 11 characters";
  } else {
    this["about"].nextElementSibling.innerHTML = "";
  }
  // Email
  if (!((/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this["email"].value)))) {
    error_data = true;
    this["email"].nextElementSibling.innerHTML = "Invalid Email";
  } else {
    this["email"].nextElementSibling.innerHTML = "";
  }
  // Contact
  if (!((/^[0-9]*$/.test(this["contact"].value)) && (this["contact"].value.length == 10))) {
    error_data = true;
    this["contact"].nextElementSibling.innerHTML = "Invalid Number";
  } else {
    this["contact"].nextElementSibling.innerHTML = "";
  }
  // zip
  if (!((/^[0-9]*$/.test(this["zip"].value)) && (this["zip"].value.length == 6))) {
    error_data = true;
    this["zip"].nextElementSibling.innerHTML = "Invalid Zipcode";
  } else {
    this["zip"].nextElementSibling.innerHTML = "";
  }
  // GitHub
  if (!((/\b(?:(?:https?|ftp):\/\/|www\.github\.com\/)([-a-z0-9+&@#\/%?=~_|!:,.;])*[-a-z0-9+&@#\/%=~_|]/i.test(this["GitHub"].value)))) {
    error_data = true;
    this["GitHub"].nextElementSibling.innerHTML = "Invalid Github URL";
  } else {
    this["GitHub"].nextElementSibling.innerHTML = "";
  }
  // Linkedin
  if (!((/\b(?:(?:https?|ftp):\/\/|www\.linkedin\.com\/)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i.test(this["LinkedIn"].value)))) {
    error_data = true;
    this["LinkedIn"].nextElementSibling.innerHTML = "Invalid LinkedIn URL";
  } else {
    this["LinkedIn"].nextElementSibling.innerHTML = "";
  }
  // Address
  if (this["address"].value.length <= 3) {
    error_data = true;
    this["address"].nextElementSibling.innerHTML = "Enter minimum 4 characters";
  } else {
    this["address"].nextElementSibling.innerHTML = "";
  }
  // Gender
  if (!($("input:radio[name='gender']").is(":checked"))) {
    error_data = true;
    this["female"].nextElementSibling.nextElementSibling.innerHTML = "Enter Gender";
  } else {
    this["female"].nextElementSibling.nextElementSibling.innerHTML = "";
  }
  //skills
  var checkedCount = $("input[type=checkbox]:checked").length;
  if (checkedCount < 3) {
    error_data = true;
    console.log("enter atleast 3");
  }
  var services = document.getElementById('interest');
  var intsel = 0;
  for (i = 0; i < services.length; i++) {
    if (services.options[i].selected) {
      intsel++;
    }
  }
  //interests
  if (intsel < 3) {
    this["interest"].nextElementSibling.innerHTML = "Enter minimum 3";
  } else {
    this["interest"].nextElementSibling.innerHTML = "";
  }
  if (error_data) {
    alert("error");
    event.preventDefault();
  }
});
